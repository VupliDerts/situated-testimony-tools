[alt text](img/STT_screenshot.png)

# Situated Testimony Tools

## About

This is a Blender addon which features a suite of tools for quickly performing operations in 3D during the process of situated testimony.  It includes assets for human figures, markers, and the ability to place crowds in the Blender scene.


## Getting started

[**You can find the latest version of Situated Testimony Tools  here.**](https://drive.google.com/open?id=1xoSivkv-yW9PEYHbNYSUE3xmdjQbK-Nj&usp=drive_fs)

### Installing

Go to Edit > Preferences, select the addons tab and choose 'install'

![alt text](img/Screenshot_2.png)

go to the 'Add-ons' tab

![alt text](img/Screenshot_3.png)

press the 'Insall Add-on from File' button

![alt text](img/Screenshot_6.png)

Select the downloaded zip file

![alt text](img/select.png)

and install it

![alt text](img/Screenshot_4.png)

You should see the addon appear in a list in the preferences window

Be sure to enable it by selecting the checkbox next to its name

![alt text](img/enable.png)
